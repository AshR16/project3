const cacheFunction= require("../cacheFunction.js")

function cb(argument){
    return argument*2
}

let result= cacheFunction(cb)

console.log(result(3))
console.log(result(4))
console.log(result(3))