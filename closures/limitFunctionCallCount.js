function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    let counter=0

    function count(){
    if (counter < n){
        counter++
        return cb()
    }
    }

   return count
   
}



module.exports=limitFunctionCallCount